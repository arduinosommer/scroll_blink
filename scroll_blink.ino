
int red = 11;
int green = 13;
int yellow = 12;

boolean a = false;
boolean o = false;
boolean e = false;

void setup() {
  //Lamp setup
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(yellow, OUTPUT);

  //Serial configuration
  Serial.begin(9600);
  while(!Serial) {}
  Serial.setTimeout(100);
  Serial.println("Arduino Initiated");
}

void turn_on(int led) {
  digitalWrite(led, HIGH);   // Sla pa strommen
}

void turn_off(int led1, int led2, int led3) {
  digitalWrite(led1,LOW);
  digitalWrite(led2,LOW);
  digitalWrite(led3,LOW);
}

void scroll_blink(int led1, int led2, int led3, int d) {
  digitalWrite(led1, HIGH);   
  delay(d);               
  digitalWrite(led1, LOW);    
  delay(d);
  digitalWrite(led2, HIGH);   
  delay(d);               
  digitalWrite(led2, LOW);    
  delay(d); 
  digitalWrite(led3, HIGH);   
  delay(d);               
  digitalWrite(led3, LOW);    
  delay(d);   
} 

void loop() {
  if(Serial.available()) {
    int inByte = Serial.read();
    Serial.println(inByte);
    switch(inByte) {
      case 'a':
        a = true;
        o = false;
        e = false;
        break;
      case 'o':
        a = false;
        o = true;
        e = false;
        break;
      case 'e':
        a = false;
        o = false;
        e = true;
        break;
      default:
        break;
    }
  }
  if(a)
    turn_on(green);
  else if(o)
    scroll_blink(green,yellow,red, 100);
  else if (e)
    turn_off(green,yellow,red);
}  
        
